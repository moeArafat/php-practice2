<?php
/**
 * @author: Mohamad Arafat
 * @comments: 
 * @purpose: A form to submit the user info
 */

    define("DBHOST", "localhost");
    define("DBCATALOG", "demo");
    define("DBUSER", "lamp1user");
    define("DBPASS", "!Lamp12!");

    function connectDB() {
        $dsn = "mysql:host=".DBHOST.";dbname=".DBCATALOG.";charset=utf8";
        try {
            $db_conn = new PDO($dsn, DBUSER, DBPASS);
            return $db_conn;
        } catch (PDOException $th) {
            echo "<p>Error opening database ".$th->getMessage()."</p>";
            exit(1);
        }
    }

function insertResponse($first_name, $last_name, $e_mail, $per_Email, $phone_Num, $per_Phone)
    {
        $db_conn = connectDB();
        $stmt = $db_conn->prepare('INSERT INTO lab3 (first_name, last_name, email, email_personal, phone, phone_personal) values(:first_name, :last_name, :e_mail, :per_Email, :phone_Num, :per_Phone)');
        if (!$stmt){
            echo "Error ".$db_conn->errorCode()."\nMessage ".implode($db_conn->errorInfo())."\n";
            exit(1);
        }
        $data = array(":first_name" =>$first_name, ":last_name"=>$last_name, ":e_mail"=>$e_mail, ":per_Email" => $per_Email, ":phone_Num" =>$phone_Num, "per_Phone" =>$per_Phone);
        $status = $stmt->execute($data);
        if(!$status){
               echo "Error ".$stmt->errorCode()."\nMessage ".implode($stmt->errorInfo())."\n";
                exit(1);
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<?php
    $firstname = $lastname = $email = $phone = $checkEmail = $checkPhone = "";
    $invalidfirstname = $invalidlastname = $invalidphone = $invalidemail = "";

    function getValue($field) {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            return $_POST[$field];
        }
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        

        if (empty($_POST['firstname'])){

            $invalidfirstname = "Your first name must not be empty"; 
        }
        else if(strlen($_POST['firstname']) > 50){
            $invalidfirstname = "Your first name must not exceed 50 character"; 
        }
        else{
            $firstname = $_POST['firstname'];
        }

        if(isset($_POST['checkEmail']) && $_POST['checkEmail'] == 'on'){
            $checkEmail = 1;
        }
        else {
            $checkEmail = 0;
        }

        if(isset($_POST['checkPhone']) && $_POST['checkPhone'] == 'on'){
            $checkPhone = 1;
        }
        else {
            $checkPhone = 0;
        }

        if (empty($_POST['lastname'])){
            $invalidlastname = "Your last name must not be empty"; 
        }
        else if(strlen($_POST['lastname']) > 50){
            $invalidlastname = "Your last name must not exceed 50 character"; 
        }
        else {
            $lastname = $_POST['lastname'];
        }

        if (empty($_POST['email'])){
             $invalidemail = "Your email address must not be empty"; 
        }
        else if(strlen($_POST['email']) > 128){
            $invalidemail = "Your email address must not exceed 128 character"; 
        }
        else {
            $email = $_POST['email'];
        }

        if (empty($_POST['phone'])){
             $invalidphone = "Your phone number must not be empty";
        }
        elseif(strlen($_POST['phone']) > 20){
            $invalidphone = "Your phone number exceed 20 character";
        }
        else {
            $phone = $_POST['phone'];
        }
       
        if (getPost('firstname') && getPost('lastname') && getPost('email') && getPost('phone')){
            insertResponse($firstname, $lastname, $email, $checkEmail, $phone, $checkPhone);
        }
    }

    function getPost($key){
        return !empty($_POST[$key]);
    }

?>

</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Import Google Icon Font-->
      
    <title>Contacts</title>
</head>
<body>
    
    <div class="container">
    
        
        <div class="emptyLine"></div>
        
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" >

            <fieldset>
                <legend>Personal Info</legend>
                
                First Name
                <br>
                <input type="text" name="firstname" id="firstname" value="<?php echo isset($_POST['firstname']) ? $_POST['firstname'] : ''; ?>">
                <span><?php echo $invalidfirstname;?></span>
                <br><br>
                Last Name
                <br>
                <input type="text" name="lastname" id="lastname" value="<?php echo isset($_POST['lastname']) ? $_POST['lastname'] : ''; ?>">
                <span><?php echo $invalidlastname;?></span>
                <br><br>
                Email
                <br>
                <input type="text" name="email" id="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>">
                <span><?php echo $invalidemail;?></span>
                <br><br>
                
                <input type="checkbox" name="checkEmail" id="perEmail" <?php echo (empty($_POST['checkEmail'])) ? '': 'checked' ?>> Is This Email Personal?
                <br><br>
                Phone Number
                <br>
                <input type="text" name="phone" id="phone" value="<?php echo isset($_POST['phone']) ? $_POST['phone'] : ''; ?>">
                <span><?php echo $invalidphone;?></span>
                <br><br>
                
                <input type="checkbox" name="checkPhone" id="perNum" <?php echo (empty($_POST['checkPhone'])) ? '': 'checked' ?>> Is This Number Personal?
                <br><br>
                <input type="submit" value="Submit" id="submitBtn">
            </fieldset>    
        </form>

    </div>

    <?php
        echo '<pre>' . print_r($_POST, true) . '</pre>';
    ?>
</body>
</html>